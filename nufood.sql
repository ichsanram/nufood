-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 16 Sep 2016 pada 08.56
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `nufood`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_makanan`
--

CREATE TABLE IF NOT EXISTS `tbl_makanan` (
  `id_makanan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_makanan` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`id_makanan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data untuk tabel `tbl_makanan`
--

INSERT INTO `tbl_makanan` (`id_makanan`, `nama_makanan`, `harga`) VALUES
(1, 'Soto Mie Bogor', 13000),
(2, 'Soto Ayam', 15000),
(3, 'Nasi Padang', 16000),
(4, 'D''Besto', 14000),
(5, 'Hisana', 14000),
(6, 'Sabana', 14000),
(7, 'Mie Ayam Kampung', 10000),
(8, 'Mie Ayam Dedi', 15000),
(9, 'Mie Ayam Ajib', 13000),
(10, 'Nasi Goreng', 12000),
(11, 'Ketoprak', 10000),
(12, 'Gado-gado', 15000),
(13, 'Ayam Bakar', 17000),
(14, 'Bakso Rawit', 15000),
(15, 'Bakso Urat', 15000),
(16, 'Pecel Ayam', 17000),
(17, 'Pecel Ati', 13000),
(18, 'Pecel Lele', 16000),
(19, 'Pecel Bebek', 23000),
(20, 'Nasi Goreng', 13000),
(21, 'jus buah', 8000),
(28, 'mm', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `kode_pemesanan` int(10) NOT NULL,
  `id_makanan` int(11) NOT NULL,
  `note` text,
  `status` int(10) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jenis_order` enum('Utama','Alternatif 1','Alternatif 2') NOT NULL,
  `jam_makan` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_makanan` (`id_makanan`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Dumping data untuk tabel `tbl_order`
--

INSERT INTO `tbl_order` (`id_order`, `id_user`, `kode_pemesanan`, `id_makanan`, `note`, `status`, `waktu`, `jenis_order`, `jam_makan`) VALUES
(117, 35, 1, 2, ' ', -10, '2016-09-05 04:24:54', 'Utama', 1),
(118, 35, 1, 3, ' ', 0, '2016-09-05 02:57:30', 'Alternatif 1', 1),
(119, 35, 1, 4, ' ', 0, '2016-09-05 02:57:38', 'Alternatif 2', 1),
(120, 48, 2, 15, ' ', -10, '2016-09-05 04:25:19', 'Utama', 1),
(121, 48, 3, 2, 'jangan pedas', 0, '2016-09-05 07:15:48', 'Utama', 0),
(122, 48, 3, 7, 'sambel nya dibanyakin ya', 0, '2016-09-05 07:16:10', 'Alternatif 1', 0),
(123, 48, 3, 11, ' pake lontong', 0, '2016-09-05 07:16:32', 'Alternatif 2', 0),
(124, 34, 4, 2, ' ', 1, '2016-09-05 07:44:41', 'Utama', 0),
(125, 36, 5, 7, 'ngg pake micin\r\nngg pake baso\r\nminta sumpit', -10, '2016-09-05 10:17:09', 'Utama', 0),
(126, 36, 5, 12, ' jangan beli yang 15rb, yang 10rb ,m jangan pake korupsi', -10, '2016-09-05 10:17:09', 'Alternatif 1', 0),
(127, 35, 6, 3, ' ga pake sambel', 0, '2016-09-05 10:38:12', 'Utama', 0),
(128, 35, 6, 1, ' ', 1, '2016-09-05 10:40:13', 'Alternatif 1', 0),
(129, 68, 7, 9, ' tanpa kuah', 0, '2016-09-14 03:56:43', 'Utama', 1),
(130, 68, 7, 2, ' tanpa kuah juga', 0, '2016-09-13 03:04:39', 'Alternatif 1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_session`
--

CREATE TABLE IF NOT EXISTS `tbl_session` (
  `id` int(11) NOT NULL,
  `ssid` varchar(50) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_topup`
--

CREATE TABLE IF NOT EXISTS `tbl_topup` (
  `id_topup` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nilai_topup` int(11) NOT NULL,
  PRIMARY KEY (`id_topup`),
  KEY `id_karyawan` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `tbl_topup`
--

INSERT INTO `tbl_topup` (`id_topup`, `id_user`, `nilai_topup`) VALUES
(2, 35, 25000),
(3, 36, 50000),
(4, 33, 50000),
(5, 35, 30000),
(6, 52, 70000),
(7, 68, 50000),
(8, 68, 10000),
(9, 68, 0),
(10, 68, -10000),
(11, 68, 0),
(12, 33, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keterangan` text,
  PRIMARY KEY (`id_transaksi`),
  KEY `id_user` (`id_user`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id_transaksi`, `id_user`, `id_order`, `harga`, `waktu`, `keterangan`) VALUES
(34, 35, 117, 15000, '2016-09-05 02:58:43', ' '),
(35, 34, 124, 15000, '2016-09-05 07:44:41', ' '),
(36, 35, 128, 14000, '2016-09-05 10:40:12', ' '),
(37, 68, 129, 13000, '2016-09-06 03:08:10', ' tanpa kuah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `saldo` float DEFAULT NULL,
  `status` enum('ob','karyawan','admin') NOT NULL,
  `token` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `nama`, `nama_lengkap`, `password`, `saldo`, `status`, `token`) VALUES
(30, 'noval', 'Noval Wahyu', '467bae90b19ee6eb379a749cb924f726', 0, 'ob', 'eb11fdcff4469dbb9df7085d2d4cf32e7482438a'),
(31, 'wahyu', 'Wahyu Maulana Rizqi', '32c9e71e866ecdbc93e497482aa6779f', 0, 'ob', 'e6f77b666ca1c8024629d98cad76bbd2459c5074'),
(32, 'hafi', 'Muhammad Hafi', 'f85f10fdcd7d8c36f39b5497ac152d89', 0, 'ob', '8b3c7af20d70a6240db0b8ff89ebc28c6336882c'),
(33, 'agus', 'Agus Setiawan', 'fdf169558242ee051cca1479770ebac3', 50000, 'karyawan', '1e90d0b3f13b2595d23657f61a8f374f85191bed'),
(34, 'ayi', 'Ayi Lestari', '3352224f8d2af6077fde6c45b60e26e9', 75000, 'karyawan', 'c80480c27c2c0558ee2507835594d47d6fdb3706'),
(35, 'arty', 'Arty Tary Martina', '84659539fc6d9efe79cb090c5df28c8c', 18000, 'karyawan', '0087c3af1dd86278ced20705d744dfe95ce1eb38'),
(36, 'bagir', 'Bagir Alfujri', '147625027cf1f1c5b281a253d1c38c65', 50000, 'karyawan', '18f063903de0f83edc296ef179deeea8c7cadadc'),
(37, 'cindy', 'Cindy', 'cc4b2066cfef89f2475de1d4da4b29c7', 0, 'karyawan', '674a3d2c7df72b5052a81a3d9dc8e4a4df2da89e'),
(38, 'dewi', 'Dewi Kartika Mala Sari', 'ed1d859c50262701d92e5cbf39652792', 0, 'karyawan', '73195cf7711beda94664ee97ada01d4fdfda232c'),
(39, 'donny', 'Donny', 'befcc07a5d7b46c63c6087f08bc601d4', 0, 'karyawan', '96e17bb5356b9ef56204b2df1a511157b0510685'),
(40, 'hana', 'Eka Hana Mulyantono', '52fd46504e1b86d80cfa22c0a1168a9d', 0, 'karyawan', '4c872435da7ca544895d25d3c0da01e19fa2b94b'),
(41, 'heri', 'Heri Ahmadi', '6812af90c6a1bbec134e323d7e70587b', 0, 'karyawan', ''),
(42, 'hafiz', 'Indah Hafiz', '839a54bf20626e4942bc8f11873f0654', 0, 'karyawan', ''),
(43, 'ircham', 'Ircham Fuadi', '30a3fc645604ac4d6871f3615232c264', 0, 'karyawan', ''),
(44, 'andiyan', 'M Andiyan Perwira', 'b30b414b5d9c1a0b597e2e9d57692410', 0, 'karyawan', '979a1b30343f8e36a8137d18c1541508729407e7'),
(45, 'popong', 'Pulong Atmoko', 'bf8dcb7dd9dd56256552fef258ebf6d0', 0, 'karyawan', '9fec84c8d71be24483a29250683be1c1dd81f815'),
(46, 'purnomo', 'Setyo Purnomo', '7392ed5e575dc6fb7b4c98422673be72', 0, 'karyawan', '398cd4d695e397ac18be2b77b2d3073fee72f864'),
(47, 'tris', 'Sutrisno', '7b1df8d14235bb3be1ea368d220bfac3', 0, 'karyawan', ''),
(48, 'subur', 'Subur Legowo', 'a1eb7a6eb40ac2fc6014cc5651636938', 90000, 'karyawan', 'f2ce09a073a4068b9c421e4a05201fad6f98a219'),
(49, 'wali', 'Syahwali', 'bf8cd26e6c6732b8df17a31b54800ed8', 0, 'karyawan', ''),
(50, 'wildan', 'Wildan Nurdiana', 'af6b3aa8c3fcd651674359f500814679', 0, 'karyawan', '5022fb5ab34eb4aa061c180a0977d247aca73f69'),
(51, 'iman', 'Iman Prihadi', '5be9a68073f66a56554e25614e9f1c9a', 0, 'karyawan', 'f5d95b35daffe2da68f5855630741b2dce09aa42'),
(52, 'miya', 'Miya', '3b650cc81ad6bfc4f7e84403c89294a8', 70000, 'karyawan', ''),
(53, 'nando', 'Nando Bonavia', '45a9a31e5f1ff59621b681a5edbffe85', 0, 'karyawan', '9b190564776d68bfa76a353a5215dd00f8d3cfe7'),
(54, 'fajar', 'Fajar Hidayat', '24bc50d85ad8fa9cda686145cf1f8aca', 0, 'karyawan', 'c196de15da03612951de9c3b0ae052e3f5cb9eee'),
(55, 'bagas', 'Bagas', 'ee776a18253721efe8a62e4abd29dc47', 0, 'karyawan', '3a76b547e1f5629fb8da2c9c79504c904cb2cca3'),
(56, 'rere', 'Rere', 'bd134207f74532a8b094676c4a2ca9ed', 0, 'karyawan', '7f1c2066b8fad1e38b161bbf349aea89bb5e1b12'),
(57, 'oka', 'Oka', 'dcf80b2416ca823e8d807558a9310eb3', 0, 'karyawan', 'baf35af8fc2342b3da37b4c5f3a119d530689aa6'),
(58, 'didot', 'Didot', 'af5734ae74ce6fd7a804a355d89e5d66', 0, 'karyawan', ''),
(59, 'bayu', 'Bayu Rama Andiyawan', 'a430e06de5ce438d499c2e4063d60fd6', 0, 'karyawan', '5e8136cfef09798cf7bbd0d2f4b84045baef6438'),
(68, 'arnas', 'Muhammad Arnas', '418d89a45edadb8ce4da17e07f72536c', 37000, 'karyawan', ''),
(73, 'dani', 'ichsan ramdani', '55b7e8b895d047537e672250dd781555', 50000, 'karyawan', ''),
(75, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 'admin', ''),
(79, 'rina', 'rina', '3aea9516d222934e35dd30f142fda18c', 0, 'ob', '7816207fd4a1ad159804b9469fe3e0120f4b31cb'),
(80, 'aaa', 'aaa', '47bce5c74f589f4867dbd57e9ca9f808', 0, 'ob', '');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD CONSTRAINT `tbl_order_ibfk_1` FOREIGN KEY (`id_makanan`) REFERENCES `tbl_makanan` (`id_makanan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_order_ibfk_3` FOREIGN KEY (`id_makanan`) REFERENCES `tbl_makanan` (`id_makanan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_order_ibfk_4` FOREIGN KEY (`id_user`) REFERENCES `tbl_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD CONSTRAINT `tbl_session_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tbl_topup`
--
ALTER TABLE `tbl_topup`
  ADD CONSTRAINT `tbl_topup_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tbl_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD CONSTRAINT `tbl_transaksi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tbl_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_transaksi_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `tbl_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_transaksi_ibfk_3` FOREIGN KEY (`id_order`) REFERENCES `tbl_order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_transaksi_ibfk_4` FOREIGN KEY (`id_user`) REFERENCES `tbl_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_transaksi_ibfk_5` FOREIGN KEY (`id_order`) REFERENCES `tbl_order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
